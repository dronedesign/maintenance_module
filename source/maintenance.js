var express = require('express');

module.exports = function maintenance(app, options) {

    var mode = false;                                           // current state, default **false**
    var endpoint = false;                                       // expose http endpoint for hot-switch, default **false**
    var switchModeUrl = '/maintenance_switch';                  // if `httpEndpoint` is on, customize endpoint url, default **'/maintenance'**
    var switchModeKey;                                          // token that client send to authorize, if not defined `access_key` is not used
    var redirectUrl = '/maintenance';                           // redirectUrl to render on maintenance, default **'maintenance.html'**
    var api = false;                                            // for rest API, species root URL to apply, default **undefined**
    var status = 503;                                           // status code for response, default **503**
    var message = 'sorry; we are on maintenance';               // response message, default **'sorry, we are on maintenance'    
    var customCallback_RequestAcceptFilter;                     // a custom async function that the user can pass, to add a custom criteria for skip. For example it could be used to provide a function that enable admin to access when in maintenance
    var customCallback_CanAcceptHotSwitchMaintenance;           // custom callback to accept/refuse hot switch of maintenance mode, by a custom criteria instead of access_key
    var skipPath = [];                                          // urls with the skipPath starting part, will be processed (and so skipped from maintenance)    
    var statusUrl = "/getstatus";


    if (typeof options === 'boolean') {
        mode = options;
    } else if (typeof options === 'object') {
        this.mode = options.current || this.mode;
        this.endpoint = options.httpEndpoint || this.endpoint;
        this.switchModeUrl = options.switchModeUrl || this.switchModeUrl;
        this.switchModeKey = options.switchModeKey;
        this.redirectUrl = options.redirectUrl || this.redirectUrl;
        this.api = options.api || this.api;
        this.status = options.status || this.status;
        this.message = options.message || this.message;
        this.skipPath = options.skipPath || this.skipPath;
        this.loginUrl = options.loginUrl || this.loginUrl;
        this.customCallback_RequestAcceptFilter = options.customCallback_RequestAcceptFilter;
        this.customCallback_CanAcceptHotSwitchMaintenance = options.customCallback_CanAcceptHotSwitchMaintenance;
        this.statusUrl = options.statusUrl || this.statusUrl;

        this.skipPath.push(this.redirectUrl);
        this.skipPath.push(this.switchModeUrl);
        this.skipPath.push(this.statusUrl);
        
    } else {
        throw new Error('unsuported options');
    }


    /**
     * 
     * Check if the current call is asking for enable/disable maintenance mode
     * 
     * @param {type} req
     * @param {type} res
     * @param {type} next
     * @returns {unresolved}
     */
    var checkIfCanAcceptHotSwitchMaintenance_ByAccessKey = function (req, res, next) {

        var errorStatus = 403;

        //The host switch is enabled?
        if (!this.switchModeUrl || !this.switchModeKey) {
            //If not but we have other method of authentication
            //then continue
            if (hasCustomCriteriaForAcceptHotSwitchRequest()) {
                return next();
            } else {
                //The swich mode is not enabled and we dont have other methods
                //so reject
                res.sendStatus(errorStatus);
            }
        }

        //Hot switch enable, check if access method is by access_key
        //If it was provided a valid access key
        //or it was provided a custom function to check if accept or not hot switch
        //then return true and go to the next function to call in the middleware stack
        var match = req.query.access_key === this.switchModeKey;
        if (match) {
            //If it's by access key and the key is correct, accept and go on
            //console.log("accepted request of switch maintenance mode with access key");
            return next();
        } else if (hasCustomCriteriaForAcceptHotSwitchRequest()) {
            //If the key is not matching (maybe because was not send), execute the other check
            return next();
        }

        //Otherwise return unauthorized error
        res.sendStatus(errorStatus);
    };

    var hasCustomCriteriaForAcceptHotSwitchRequest = function () {
        return typeof this.customCallback_CanAcceptHotSwitchMaintenance === 'function';
    }

    var checkIfCanAcceptHotSwitchMaintenance_ByCustomCriteria = function (req, res, next) {

        if (hasCustomCriteriaForAcceptHotSwitchRequest()) {
            //console.log("checking accept request of switch maintenance mode with custom strategy");
            this.customCallback_CanAcceptHotSwitchMaintenance(req, res, next);
        }

        next();
    };


    /**
     * You can customize this function from the outside
     * if you want to add a custom criteria for call be able to pass
     * when in maintenance mode
     * 
     * If this function return true, the request will not be redirected to maintenance
     * 
     */
    var customRequestAcceptFilter = function (req) {

        if (typeof this.customCallback_RequestAcceptFilter === "function") {
            return this.customCallback_RequestAcceptFilter(req);
        }

        return false;
    }


    /**
     * Add routers for hot switch maintenance mode
     * 
     * @param {type} app
     * @returns {nm$_maintenance.module.exports}
     */
    var server = function (app) {
        if (this.endpoint) {
            var router = express.Router();


            if (this.statusUrl) {
                //route for checkStatus
                router.get(this.statusUrl, function (req, res) {
                    var status = this.mode ? "on" : "off";
                    //console.log("check status: " + status);
                    res.send({"status": status});
                });
            }


            if (this.switchModeUrl) {
                //route for mode enabling
                router.post(this.switchModeUrl, checkIfCanAcceptHotSwitchMaintenance_ByAccessKey, checkIfCanAcceptHotSwitchMaintenance_ByCustomCriteria, function (req, res) {
                    //console.log("enabling maintenance mode");
                    this.mode = true;
                    res.sendStatus(200);
                });


                //route for mode disabling
                router.delete(this.switchModeUrl, checkIfCanAcceptHotSwitchMaintenance_ByAccessKey, checkIfCanAcceptHotSwitchMaintenance_ByCustomCriteria, function (req, res) {
                    //console.log("disabling maintenance mode");
                    this.mode = false;
                    res.sendStatus(200);
                });
            }


            if (this.statusUrl || this.switchModeUrl) {
                app.use('/', router);
            }

        }

        return this;
    };


    /**
     * Handle the request when in maintenance mode
     * If the request is coming from an api, we will reply with a json
     * Otherwise we will redirect the user to maintenance page
     * 
     * @param {type} req
     * @param {type} res
     * @returns {unresolved}
     */
    var handle = function (req, res) {

        var isApi = this.api && req.url.indexOf(this.api) === 0;

        res.status(this.status);

        if (isApi) {
            return res.json({message: message});
        }

        return res.redirect(this.redirectUrl);
    };


    /**
     * Check wheter or not the current request has to be handled by maintenance page
     * Or has to be allowed
     * 
     * If the current request.url start with a string in skipPath then the current request
     * will be processed instead of being redirected to the maintenance page
     * 
     * @param {type} url
     * @returns {Boolean}
     */
    var checkIsSkipPath = function (url) {

        if (!this.skipPath || this.skipPath.length == 0) {
            return false;
        }

        for (var i in this.skipPath) {
            var path = this.skipPath[i];
            var isSkipPath = url.indexOf(path) === 0; //if the url start with path
            if (isSkipPath) {
                return true;
            }
        }

        return false;
    };

    /**
     * If maintenance mode is enable
     * And this request has not to be skipped (and so processed)
     * then handle the maintenance call
     * Otherwise go on
     * 
     * 
     */
    var middleware = function (req, res, next) {

        if (this.mode) {
            customRequestAcceptFilter(req).then(customFilterTellToSkip => {

                var hasToSkipThisPath = checkIsSkipPath(req.url);

                var redirectToMaintenance = !customFilterTellToSkip && !hasToSkipThisPath; //&& this.mode

                if (redirectToMaintenance) {
                    return handle(req, res);
                } else {
                    next();
                }
            });
        } else {
            next();
        }

    };

    /**
     * 
     * Attach the middleware to the app
     * 
     * @param {type} app
     * @returns {unresolved}
     */
    var inject = function (app) {
        app.use(middleware);
        return app;
    };


    /**
     * In one call
     * Attach the middleware to the app
     * Attach the routes for hot switch
     * Return this object
     */
    return server(inject(app));

};